package com.mercadolivre.testesimios.libs;

import java.util.ArrayList;

public class Simian {	
	public boolean isBaseDNA(String[] dna_base) {
		for (String dna : dna_base) {
			 if (!( (dna != null) && (!dna.equals("")) && (dna.matches("(?i).*^[atcg]*$.*")) )) {
				 return false;
			 }; 
					
		}
		return true;
	}
	
	public boolean isSimian(String[] dna) {
		if(!this.isBaseDNA(dna)) {
			return false;
		}
		int dnas_finded = 0;
		String[][] dna_normalized = normalizeArr(dna);	
		ArrayList<String> diagonal_dna_finded = new ArrayList<String>();		
		
		for (int i = 0; i < dna_normalized.length; i++) {
			if(dnas_finded > 1) {
				return true;
			}
			int count_lines_finded = 1;			
			int count_horizontal_finded = 0;
			
			for (int j = 0; j < dna_normalized.length; j++) {					
				
				if(j+1 < dna_normalized.length && dna_normalized[i][j].equals(dna_normalized[i][j+1])) {
					count_lines_finded ++;
					if(count_lines_finded >=4) {
						dnas_finded++;
					}					
				}
				
				if(j+1 < dna_normalized.length && dna_normalized[j][i].equals(dna_normalized[j+1][i])) {					
					count_horizontal_finded ++;
					if(count_horizontal_finded >=4) {	
						dnas_finded++;
					}
					
				}
				
			}
			
		}
		
		int count_diagonal_finded = 1;
		
		for (int k = 0; k <= 2 * (dna_normalized.length - 1); ++k) {
		    for (int y = 0; y < dna_normalized.length; ++y) {
		        int x = k - y;
		        if (x < 0 || x >= dna_normalized.length) {		           
		        } else {
		        	diagonal_dna_finded.add(dna_normalized[y][x]);
		        	//System.out.print(dna_normalized[y][x]);
		        
		        }
		    }
		    
		}
		
		for (int i = 0; i < diagonal_dna_finded.size(); i++) {
			if(dnas_finded > 1) {
				return true;
			}
			int size = diagonal_dna_finded.size();
			if(i+1 < size && diagonal_dna_finded.get(i).equals(diagonal_dna_finded.get(i+1))) {
				count_diagonal_finded++;
				if(count_diagonal_finded >=4) {	
					
					dnas_finded++;
				}
			} else {
				count_diagonal_finded = 1;
			}
			
		}		
		
		return false;
	}
	
	private String[][] normalizeArr(String[] dna) {
		String[][] result = new String[6][6];
		
		for (int i = 0; i < dna.length; i++) {
			result[i] = dna[i].split("");
		}		
		
		return result;
		
	}
	

}
