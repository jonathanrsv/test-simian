package com.mercadolivre.testesimios;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.google.gson.Gson;
import com.mercadolivre.testesimios.libs.Simian;
import com.mercadolivre.testesimios.model.Dna;
import com.mercadolivre.testesimios.repositories.DnaRepository;

import java.util.List;


@RestController
@RequestMapping( "/simian")
@SpringBootApplication 
@EnableAutoConfiguration(exclude = { JacksonAutoConfiguration.class })

public class TestesimiosApplication {	
	
	@Autowired
	private DnaRepository dnaRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(TestesimiosApplication.class, args);
	}
	

    @GetMapping
	public String ndex() {
 
    	List<Dna> dnashumans = dnaRepository.findByType("human");
    	List<Dna> dnassimmian = dnaRepository.findByType("simian");
    	int totalhumans = dnashumans.size();
    	int totalsimian = dnassimmian.size();
    	
    	String jsonToReturn = new JSONObject()
    			.put("count_mutant_dna", String.valueOf(totalsimian))
                .put("count_human_dna", String.valueOf(totalhumans))
                .put("ratio", 1)
                .toString();    			
    	
    	return jsonToReturn;       
    }
	
	
	@PostMapping(consumes = "application/json")
	public ResponseEntity<String> isSimian(@RequestBody Dna dna) {	
		
		Simian simian = new Simian();	
		
		
		if(simian.isSimian(dna.getDna())) {
		
			String json = new Gson().toJson(dna.getDna() );		
			
			Dna dnaobj = new Dna();				
			dnaobj.setDna(dna.getDna());
			dnaobj.setDnaJon(json);
			dnaobj.setType("simian");
			dnaRepository.save(dnaobj);
			
			return new ResponseEntity<>(HttpStatus.OK);
			
		} 
		
		if (simian.isSimian(dna.getDna()) == false && simian.isBaseDNA(dna.getDna()) == true)	{
			
			String json = new Gson().toJson(dna.getDna() );		
			
			Dna dnaobj = new Dna();				
			dnaobj.setDna(dna.getDna());
			dnaobj.setDnaJon(json);
			dnaobj.setType("human");
			dnaRepository.save(dnaobj);
			
			
		}
		
		return new ResponseEntity<>(HttpStatus.FORBIDDEN);		
		           
	}
	
	

}
