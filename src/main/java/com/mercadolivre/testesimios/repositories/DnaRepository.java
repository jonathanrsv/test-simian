package com.mercadolivre.testesimios.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercadolivre.testesimios.model.Dna;


public interface  DnaRepository  extends JpaRepository<Dna, Long>{

	List<Dna> findByType(String string);

}
