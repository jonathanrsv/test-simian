package com.mercadolivre.testesimios.model;

import javax.persistence.*;


@Entity
@Table(
name="dna",
uniqueConstraints={@UniqueConstraint(columnNames="dnaJon")}
)

public class Dna {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String type;
	
	@Basic
    @Column(nullable=false, length=150)
	private String dnaJon;
	
	@Transient
	private String[] dna;
	
	public String getDnaJon() {
		return dnaJon;
	}

	public void setDnaJon(String dnaJon) {
		this.dnaJon = dnaJon;
	}	


	public String[] getDna() {
		return dna;
	}
	


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setDna(String[] dna) {
		this.dna = dna;
	}


	
}
